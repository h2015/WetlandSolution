﻿using Microsoft.Extensions.Configuration;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using Wetland.Models;

namespace Wetland.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //创建实例方式 1
                //string r = "data source=218.249.12.119,1599;uid=netdb;pwd=qwer135@$Dbs;";
                //string w = "data source=218.249.12.119,1599;uid=netdb;pwd=qwer135@$Dbs;";
                //string SYS_Wetland_DB = "SYS_Wetland_DB";
                //string SYS_Wetland_Rel = "SYS_Wetland_Rel";
                //MassiveContext<YY_DATA_AUTO> context = new MassiveContext<YY_DATA_AUTO>(r, w, SYS_Wetland_DB, SYS_Wetland_Rel);

                /*创建实例方式 2
                        <connectionStrings>
                           <add name="SYS_Wetland_DBWrite" connectionString="data source=.;uid=sa;pwd=123456" providerName="System.Data.SqlClient" />
                           <add name="SYS_Wetland_DBReader" connectionString="data source=.;uid=sa;pwd=123456" providerName="System.Data.SqlClient" />
                        </connectionStrings>
                        <appSettings>
                           <add key="SYS_Wetland_DB" value="SYS_Wetland_DB"/>
                           <add key="SYS_Wetland_Rel" value="SYS_Wetland_Rel"/>
                        </appSettings>
                */
                MassiveContext<YY_DATA_AUTO> context = new MassiveContext<YY_DATA_AUTO>();

                //for (int i = 0; i < 10; i++)
                //{
                //    YY_DATA_AUTO entity = new YY_DATA_AUTO();
                //    entity.STCD = System.Guid.NewGuid().ToString();
                //    entity.ItemID = "001";
                //    entity.TM = DateTime.Now;
                //    entity.DATAVALUE = 10;
                //    entity.DOWNDATE = DateTime.Now;
                //    entity.STTYPE = "";
                //    entity.NFOINDEX = 1;
                //    entity.CorrectionVALUE = 11;
                //    context.Add(entity);
                //    Console.WriteLine(entity.STCD + "写入成功!!...");
                //}

                Expression<Func<YY_DATA_AUTO, bool>> where = p => true;//p => p.TM > startTime && p.TM <endTime;
                DBPageBase page = new DBPageBase();
                page.PageIndex = 1;
                page.PageSize = 1000;
                page.OrderBy = " ORDER BY WetlandID ASC ";
                Stopwatch sw = new Stopwatch();
                sw.Start();
                var result = context.Get(where, page);
                sw.Stop();
                Console.WriteLine("用时:" + sw.ElapsedMilliseconds);
                result.ForEach(f => Console.WriteLine(f.TM));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
