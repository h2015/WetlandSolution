﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wetland.SqlServer
{
    public class SqlServerDatabase
    {
        public static string Writer
        {
            get
            {
                try
                {
                    var builder = new ConfigurationBuilder()
                                .SetBasePath(Directory.GetCurrentDirectory())
                                .AddJsonFile("appsettings.json");
                    var config = builder.Build();
                    string writer= config["Wetland:SYS_Wetland_DBWrite"];
                    return writer;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
        public static string Reader
        {
            get
            {
                try
                {
                    var builder = new ConfigurationBuilder()
                                 .SetBasePath(Directory.GetCurrentDirectory())
                                 .AddJsonFile("appsettings.json");
                    var config = builder.Build();
                    string reader = config["Wetland:SYS_Wetland_DBReader"];
                    return reader;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static string GetAppSetting(string key)
        {
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            var config = builder.Build();
            string setting = config["Wetland:"+key+""];
            return setting;
        }
    }
}
